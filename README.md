# Lita-Rubygems-Webhooks

Lita-Rubygems-Webhooks allows a Lita bot to register and receive webhooks from Rubygems, alerting a chat channel when new versions of gems are released

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'lita-rubygems-webhooks'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install lita-rubygems-webhooks

## Webhooks Registration

The RubyGems platform requires that webhooks be registered via it's [API](http://guides.rubygems.org/rubygems-org-api/) so therefore the Lita handler has to register it's own webhooks upon startup. This plugin therefore requires API credentials in order to operate. The bot will _only_ register & deregister webhooks that are associated with it's domain/URL

## Configuration

### Required attributes
- `api_key` (`String`) - The RubyGems API key to register webhooks with RubyGems (see above)
- `domain` (`String`) - The domain/IP address of the Lita bot, used for registering itself with RubyGems. URLS will be created of the form `http://#{domain}/rubygems`
- `gems` (`Array<String>`) - A list of gems to watch for new pushes
- `alert_channel` (`String`) - The chat channel to send messages to when new versions are published

### Example
```
Lita.configure do |config|
  config.handlers.rubygems_webhooks.api_key = "d34db33f"
  config.handlers.rubygems_webhooks.domain = 'lita.example.com'
  config.handlers.rubygems_webhooks.gems = ["rails"]
  config.handlers.rubygems_webhooks.alert_channel = 'shell'
end
```

## Usage

1. Setup which gems and room you wish to register for RubyGems observations
2. The bot can respond with which gems it is watching with a direct message/command of `rubygems`

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`.

## Contributing

Bug reports and pull requests are welcome on [GitLab](https://gitlab.com/adambuckland/lita-rubygems-webhooks).


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

