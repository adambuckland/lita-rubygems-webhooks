# frozen_string_literal: true
require 'gems'

module Lita
  module Handlers
    module RubyGemsWebhooks
      module Registrar
        class << self
          attr_accessor :registrar_config
        end

        def self.configure
          self.registrar_config ||= {}
          yield(self.registrar_config)
          Gems.configure do |config|
            config.key = self.registrar_config['api_key']
          end
        end

        def self.configure_web_hooks
          remote_hooks = web_hooks
          desired_hooks = self.registrar_config['hooks']
          current_hooks = filter_webhooks(remote_hooks, self.registrar_config['domain'])

          new_hooks = desired_hooks - current_hooks
          old_hooks = current_hooks - desired_hooks

          register_hooks(new_hooks, self.registrar_config['domain'])
          deregister_hooks(old_hooks, self.registrar_config['domain'])
        end

        def self.register_hooks(gem_names, domain)
          gem_names.each do |gem_name|
            register_hook(gem_name, domain)
          end
        end

        def self.deregister_hooks(gem_names, domain)
          gem_names.each do |gem_name|
            deregister_hook(gem_name, domain)
          end
        end

        def self.register_hook(gem_name, domain)
          Gems.add_web_hook(gem_name, webhook_url(domain))
        end

        def self.deregister_hook(gem_name, domain)
          Gems.remove_web_hook(gem_name, webhook_url(domain))
        end

        def self.web_hooks
          Gems.web_hooks
        end

        # Example output
        # {
        #     "rails": [
        #         {
        #             "failure_count": 0,
        #             "url": "http://3195fa79.ngrok.io/payload"
        #         },
        #         {
        #             "failure_count": 0,
        #             "url": "http:/google.com/payload"
        #         }
        #     ]
        # }
        def self.filter_webhooks(hooks, domain, registered: true)
          hooks.select do |_, gem_hooks|
            if registered
              gem_hooks.any? { |hook| hook['url'].include?(domain) }
            else
              !gem_hooks.any? { |hook| hook['url'].include?(domain) }
            end
          end.keys.map(&:to_s)
        end

        def self.webhook_url(domain)
          "http://#{domain}/rubygems"
        end

        def self.clear_config
          self.registrar_config = {}
        end
      end
    end
  end
end
