# frozen_string_literal: true
require 'digest'
require 'json'

module Lita
  module Handlers
    module RubyGemsWebhooks
      class WebhookHandler < Lita::Handler
        class << self
          def name
            'RubygemsWebhooks'
          end
        end

        # Rubygems API key
        config :api_key, type: String, required: true
        # Domain of the Lita bot, where webhooks will be registered to
        config :domain, type: String, required: true
        # Array of gems to track
        config :gems, type: Array, required: true
        # (Optional) Chat channel to alert
        config :alert_channel, type: String, required: true

        route 'rubygems', :respond, command: true

        on :loaded, :start_registrar

        http.post('/rubygems', :handle_webhook)

        def respond(response)
          response.reply index_message
        end

        def handle_webhook(request, response)
          gem_info = JSON.parse(request.body.read)
          auth_header = request.env['HTTP_AUTHORIZATION']
          unless authentic_token?(auth_header, gem_info['name'], gem_info['version'], config.api_key)
            return handle_unauthentic_token(response)
          end
          target = Source.new(room: config.alert_channel)
          robot.send_messages(target, update_message(gem_info))
          response.status = 200
          response
        end

        def start_registrar(_payload)
          Registrar.configure do |configuration|
            configuration['api_key'] = config.api_key
            configuration['domain'] = config.domain
            configuration['hooks'] = config.gems
          end
          Registrar.configure_web_hooks
        end

        private

        # An authentic Authorization token will be the SHA2 of
        # gem_name, gem_version and API key
        def authentic_token?(presented_token, gem_name, gem_version, api_key)
          false if [presented_token, gem_name, gem_version, api_key].include?(nil)
          expected_token = Digest::SHA256.new
          expected_token << (gem_name + gem_version + api_key)
          expected_token == presented_token
        end

        def handle_unauthentic_token(response)
          response.status = 403
          response
        end

        def index_message
          <<~EOS
          #{robot.name} is currently observing the following gems:
          #{'- ' + gems.join("\n- ")}
          EOS
        end

        def update_message(gem_info)
          <<~EOS
          #{gem_info['name']} gem has been updated!
          #{gem_info['info']}
          New version: #{gem_info['version']}
          EOS
        end

        def gems
          config.gems
        end
      end
      Lita.register_handler(WebhookHandler)
    end
  end
end
