# frozen_string_literal: true
module Lita
  module Handlers
    module RubyGemsWebhooks
      class << self
      end
    end
  end
end

require_relative 'rubygems_webhooks/webhook_handler'
require_relative 'rubygems_webhooks/registrar'

# Lita::Handlers::RubyGemsWebhooks::Registrar.configure_web_hooks
