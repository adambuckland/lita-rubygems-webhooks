# frozen_string_literal: true
$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'rspec'
# require 'rack-test'
require 'lita/rspec'
require 'lita/handlers/rubygems_webhooks'
require 'byebug'

Lita.version_3_compatibility_mode = false

# If we're on GitLab CI, we need to set to use
# Redis Docker service

if ENV.key?('GITLAB_CI')
  Lita.configure do |config|
    config.redis[:host] = 'redis'
  end
end
