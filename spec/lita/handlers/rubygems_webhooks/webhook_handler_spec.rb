# frozen_string_literal: true
require 'spec_helper'
require 'digest'

describe Lita::Handlers::RubyGemsWebhooks::WebhookHandler, lita_handler: true do
  let(:domain) { 'google.com' }
  let(:api_key) { 'DEADBEEF' }
  let(:gems) { %w(rails praxis) }
  let(:alert_channel) { 'shell' }
  let(:stubbed_registrar) { class_double('Lita::Handlers::RubyGemsWebhooks::Registrar').as_stubbed_const }
  let(:config) { Lita.config.handlers.rubygems_webhooks }

  before do
    config.api_key = api_key
    config.domain = domain
    config.gems = gems
    config.alert_channel = alert_channel
    # We don't want to fire these
    expect(stubbed_registrar).to receive(:configure)
    expect(stubbed_registrar).to receive(:configure_web_hooks)
  end

  describe 'event handling' do
    describe('command routes') do
      it { is_expected.to route_command('rubygems').to :respond }
    end

    describe 'HTTP routes' do
      it { is_expected.to route_http(:post, '/rubygems').to :handle_webhook }
    end

    describe 'Event routes' do
      it { is_expected.to route_event(:loaded).to :start_registrar }
    end
  end

  describe '#respond' do
    before do
      send_command('rubygems')
    end

    it 'lists the observed gems' do
      expect(replies.last).to eq(
        <<~EOS
        Lita is currently observing the following gems:
        - rails
        - praxis
      EOS
      )
    end
  end

  describe '#handle_webhook' do
    let(:gem_name) { 'rails' }
    let(:gem_version) { '9000.0' }
    let(:gem_info) { 'Everyone\'s favourite web framework' }
    let(:payload) do
      JSON.generate(name: gem_name,
                    version: gem_version,
                    info: gem_info)
    end
    subject { http.post('rubygems', payload, authorization: auth_token) }
    context 'with valid authorization token' do
      let(:auth_token) do
        token = Digest::SHA256.new
        token << gem_name + gem_version + api_key
      end

      it 'returns the correct status code' do
        expect(subject.status).to eq(200)
      end

      it 'prints the correct message' do
        subject # trigger
        expect(replies.last).to eq(
          <<~EOS
          rails gem has been updated!
          Everyone's favourite web framework
          New version: 9000.0
          EOS
        )
      end
    end

    context 'without a valid authorization token' do
      let(:auth_token) { Digest::SHA256.new << 'foobar' }
      it 'returns the correct status code' do
        expect(subject.status).to eq(403)
      end

      it 'returns no message' do
        subject
        expect(replies.last).to be_nil
      end
    end
  end
end
