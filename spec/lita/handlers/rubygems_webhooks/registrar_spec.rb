# frozen_string_literal: true
require 'spec_helper'

module Lita
  module Handlers
    module RubyGemsWebhooks
      describe Registrar do
        let(:api_key) { 'TEST' }
        let(:domain) { 'google.com' }
        let(:expected_url) { "http://#{domain}/rubygems" }
        let(:gems) { ['rest-client', 'octokit'] }
        let(:example_webhooks) do
          {
            'rails' => [
              {
                'failure_count' => 0,
                'url' => 'http://3195fa79.ngrok.io/rubygems'
              },
              {
                'failure_count' => 0,
                'url' => 'http:/google.com/rubygems'
              }
            ],
            'rest-client' => [
              {
                'failure_count' => 0,
                'url' => 'http://3195fa79.ngrok.io/rubygems'
              }
            ]
          }
        end

        before do
          Registrar.configure do |config|
            config['api_key'] = api_key
            config['domain'] = domain
            config['hooks'] = gems
          end
        end

        describe '#webhooks' do
          subject { Registrar.web_hooks }

          it 'fetches webhooks from rubygems' do
            expect(Gems).to receive(:web_hooks).and_return(example_webhooks)
            subject
          end
        end

        describe '#register_hook' do
          let(:gem_name) { 'rails' }

          subject { Registrar.register_hook(gem_name, domain) }

          it 'registers hook with rubygems' do
            expect(Gems).to receive(:add_web_hook).with(gem_name, expected_url)
            subject
          end
        end

        describe '#deregister_hook' do
          let(:gem_name) { 'rails' }

          subject { Registrar.deregister_hook(gem_name, domain) }

          it 'deregisters hook with rubygems' do
            expect(Gems).to receive(:remove_web_hook).with(gem_name, expected_url)
            subject
          end
        end

        describe '#register_hooks' do
          let(:gems) { ['rails', 'rest-client'] }

          subject { Registrar.register_hooks(gems, domain) }

          it 'registers hooks with rubygems' do
            expect(Gems).to receive(:add_web_hook).exactly(2).times
            subject
          end
        end

        describe '#deregister_hooks' do
          let(:gems) { ['rails', 'rest-client'] }

          subject { Registrar.deregister_hooks(gems, domain) }

          it 'deregisters hooks with rubygems' do
            expect(Gems).to receive(:remove_web_hook).exactly(2).times
            subject
          end
        end

        describe '#configure_web_hooks' do
          before do
            expect(Registrar).to receive(:web_hooks).and_return(example_webhooks)
            expect(Registrar).to receive(:register_hook).exactly(2).times
            expect(Registrar).to receive(:deregister_hook).exactly(1).times
          end

          subject { Registrar.configure_web_hooks }

          it 'registers the correct hooks' do
            subject
          end
        end

        describe '#filter_webhooks' do
          let(:hooks) { example_webhooks }
          subject { Registrar.filter_webhooks(hooks, domain, registered: registered) }

          context 'when looking for registered hooks' do
            let(:filtered_hooks) { ['rails'] }
            let(:registered) { true }
            it 'correctly identitifes the registered webhooks' do
              is_expected.to eq(filtered_hooks)
            end
          end

          context 'when looking for unregistered hooks' do
            let(:filtered_hooks) { ['rest-client'] }
            let(:registered) { false }
            it 'correctly identitifes the unregistered webhooks' do
              is_expected.to eq(filtered_hooks)
            end
          end
        end

        after do
          Registrar.clear_config
        end
      end
    end
  end
end
